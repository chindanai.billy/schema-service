const jwt = require('jsonwebtoken');

const validate = async (headers, secretKey) => {
  try {
    let data = {}
    let error = null

    const bearer = headers.authorization
    if (bearer) {
      const token = bearer.slice(7); // Bearer token...
      const key = new Buffer.from(secretKey, 'base64')
      jwt.verify(token, key,
        {
          header: { TYP: 'JWT' },
          algorithms: ['HS256']
        },
        function (err, decoded) {
          error = err
          data = decoded || {}
        }
      );
    } else {
      error = true
    }

    if (error) {
      console.log('---- jwt.verify error ----');
      console.log(error.message);
      data = { failed: error.message }
    }

    return {
      authValid: error === null ? true : false,
      authData: data
    }
  } catch (error) {
    return {
      authValid: false,
      authData: {}
    }
  }
}


module.exports = {
  validate,

}
